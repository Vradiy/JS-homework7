let userChoice = prompt('Enter some property!','name');

const users = [{
    name: 'Max',
    surname: 'Chubko',
    gender: 'male',
    age: 35
  },
  {
      name: 'Stas',
      surname: 'Gera',
      gender: 'female',
      age: 22
  },
  {
    name: 'Bogdan',
    surname: 'Sivashenko',
    gender: 'female',
    age: 23
}
]

const alternateUsers = [{
    name: 'Rodion',
    surname: 'Chubko',
    gender: 'female',
    age: 35
  },
  {
      name: 'Sasha',
      surname: 'Faydish',
      gender: 'male',
      age: 27
  },
  {
    name: 'Max',
    surname: 'Sivashenko',
    gender: 'male',
    age: 23
}];

function excludeBy(firArray, secArray,field){
    let filteredArray = [];
    firArray.forEach(function(item){
        let flag = true;
    secArray.forEach(function(secItem){
        if(item[field] === secItem[field]){
            flag = false;
        }
    })
        if(flag){
            filteredArray.push(item);
        }
    })
    return filteredArray;
};

console.log(excludeBy(users, alternateUsers, userChoice));


// let keks = [userChoice];

// let func = () => "asd" тоже самое что и ниже написанно.

// let func = () => {
//     return "asd"
// }

////////////////ВТОРОЙ ВАРИАНТ//////////////////
// function excludeBy(mainArr, excludeArr,field){
//     return mainArr.filter( o => {
//         return excludeArr.every(o2 => {
//            return o[field] !== o2[field];
//         });
//     })
// }


///////////////ТРЕТИЙ ВАРИАНТ//////////////////
// function excludeBy(){
//     let result = users.filter(function(o){
//     return !alternateUsers.some(function(o2){
//        return o[keks] === o2[keks];
//     });
// }).map(function(o){
//     return keks.reduce(function(newo, userChoice){
//         newo[userChoice] = o[userChoice];
//         return newo;
//     }, {});
// });
// return result;
// };
// console.log(excludeBy(users,alternateUsers,userChoice));